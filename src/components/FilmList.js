import React from 'react';
import { connect } from 'react-redux';
import { Film } from './Film';

const FilmList = (props) => (
    <div>
        FilmList:
        <ul>
            {props.films.map(film => {
                return(
                    <li key={film.id}>
                        <Film {...film} />
                    </li>
                );
            })}
        </ul>
    </div>
);

const mapStateToProps = (state) => {
    return {
        books: state
    };
};

export default connect(mapStateToProps)(FilmList);
