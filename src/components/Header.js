import React from 'react';
import { NavLink } from 'react-router-dom';


const Header = () => (
    <header>
        <h2>Spring, React, Redux, PostgreSQL</h2>
        <h4>Film DB Application</h4>
        <div className='header__nav'>
            <NavLink to="/" activeClassName='activeNav' exact={true}>DashBoard</NavLink>
            <NavLink to="/add" activeClassName='activeNav'>AddFilm</NavLink>
        </div>
    </header>
);

export default Header;