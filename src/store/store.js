import { createStore, applyMiddleware } from "redux";
import films from '../reducers/films';
import thunk from 'redux-thunk';

export default () => {
    return createStore(films, applyMiddleware(thunk));
}