import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import GetAppStore from './store/store';
import { getFilms } from './actions/films';

import { Provider } from 'react-redux';

const store = GetAppStore();

const template = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

store.dispatch(getFilms()).then(() => {
    ReactDOM.render(template, document.getElementById('app'));
});